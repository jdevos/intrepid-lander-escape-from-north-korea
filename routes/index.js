/*
 * index for home, scratch, and sandbox
 */

module.exports = {
    index: function (req, res) {
        res.render('index', { title: 'Intrepid Lander' });
    },

    scratch: function (req, res) {
        res.render('scratch', { title: 'Intrepid Lander' });
    },

    sandbox: function (req, res) {
        var data = {
            title: "Box2D Sandbox",
            defaults: {
            }
        };
        res.render('sandbox', data);
    }
 }
