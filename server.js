var express = require('express');
var routes = require('./routes/index');

var app = express();

app.engine('jade', require('jade').__express);

app.set('views', './views');
app.set('view engine', 'jade');

app.use(express.static('public'));

app.get('/', routes.index);
app.get('/scratch', routes.scratch);
app.get('/sandbox', routes.sandbox);

if (app.get('env') === 'development') {
  app.locals.pretty = true;
}

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
