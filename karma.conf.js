// Karma configuration
// Generated on Wed Nov 25 2015 11:50:09 GMT-0700 (MST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'public/components/Box2dWeb-2.1.a.3.js',
      'public/js/**/core.js',
      'public/js/**/xhr.js',
      'public/js/**/Timer.js',
      'public/js/**/Factory.js',
      'public/js/**/Constants.js',
      'public/js/**/PhysicsEngine.js',
      'public/js/**/AssetLoader.js',
      'public/js/**/RenderEngine.js',
      'public/js/**/TileMap.js',
      'public/js/**/SpriteSheet.js',
      'public/js/**/SpriteSheetAnim.js',
      'public/js/**/DeviceOrientation.js',
      'public/js/**/InputEngine.js',
      'public/js/**/Entity.js',
      'public/js/**/LanderEntity.js',
      //'public/js/**/main.js',
      
      'test/public/**/*Spec.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    //browsers: ['Chrome', 'Safari'],
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultanous
    concurrency: Infinity
  })
}
