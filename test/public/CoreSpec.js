describe("core functions", function(){
	
	it("properly extends objects", function(){
		var obj1 = {};
		var obj2 = {a: 1, b:'two', inner:{aa:11, bb:'twotwo'}};
		var obj3 = core_extend({}, obj2);
		
		expect(obj3.a).toBeDefined();
		
		expect(obj3.a).toBe(obj2.a);
		expect(obj3.b).toBe(obj2.b);
		expect(obj3.inner).toBe(obj2.inner);
	});
	
	
});