{"frames": {

"shipa-large.png":
{
	"frame": {"x":2,"y":2,"w":128,"h":128},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":128},
	"sourceSize": {"w":128,"h":128}
},
"shipa-small.png":
{
	"frame": {"x":132,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"thrust-full.png":
{
	"frame": {"x":198,"y":2,"w":128,"h":128},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":128},
	"sourceSize": {"w":128,"h":128}
},
"thrust-left.png":
{
	"frame": {"x":328,"y":2,"w":128,"h":128},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":128},
	"sourceSize": {"w":128,"h":128}
},
"thrust-light.png":
{
	"frame": {"x":2,"y":132,"w":128,"h":128},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":128},
	"sourceSize": {"w":128,"h":128}
},
"thrust-medium.png":
{
	"frame": {"x":132,"y":132,"w":128,"h":128},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":128},
	"sourceSize": {"w":128,"h":128}
},
"thrust-right.png":
{
	"frame": {"x":262,"y":132,"w":128,"h":128},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":128},
	"sourceSize": {"w":128,"h":128}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "ship-assets.png",
	"format": "RGBA8888",
	"size": {"w":512,"h":512},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:79ace3f20ed63caf054b2cb79dccf3cd$"
}
}
