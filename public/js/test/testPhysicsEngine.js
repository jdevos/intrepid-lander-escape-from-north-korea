//TODO: convert these to karma tests


test("should exist", function () {
	ok(new PhysicsEngineClass());
});

test("world scaling", function () {
	var pe = new PhysicsEngineClass({initialScale: 0.5});
	ok(pe, "physics engine defined");
	var scalepos = pe.getScaledPosition({x:1.0, y:1.0});
	var expected = {x: 0.5, y: 0.5};
	deepEqual(scalepos, {x: 0.5, y: 0.5}, "scaled position was not in expected location" );
});