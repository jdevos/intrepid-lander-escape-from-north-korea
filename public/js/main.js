$(function () {
    console.log('$.ready ');

    $(window).keydown(function (evt) {
        //console.log("captured: %s", evt.which);
        if (evt.which === KEY.ESC) {
            $('#debug').slideToggle({easing: "swing", duration: 250});
        }
        ;
    });

    var map = null;

    var assets = [
        "/assets/level-test.js",
        "/assets/level-test-2.js",
        "/assets/ship-assets.js",
        "/assets/ship-assets.png",
        "/assets/awful-tileset.png",
        "/assets/level-test-3-with-bounding.js",
        "/assets/map1.js",
        "/assets/shipa-large.js"
    ];

    var loading = gAssetLoader.loadAssets(assets);
    loading.done(function (cache) {
        console.log("done loading assets!");
        var map = gAssetLoader.cache["/assets/level-test-2.js"];
        gTileMap.load(map, "/assets/").done(main);
    });
});

function main() {
    gInputEngine.setup();
    gRenderEngine.setup($('#gamecanvas')[0], Constants.CANVAS_WIDTH, Constants.CANVAS_HEIGHT);
    gGameEngine.debugPhysics = false;
    gGameEngine.setup();

    test3();
}

var updateDebugWindow = function () {
    var txt = "";
    for (var action in gInputEngine.actions) {
        if (gInputEngine.actions[action]) {
            txt += " " + action;
        }
        txt += "\nfuel:" + gGameEngine.player.fuel;
    }
    $('#debug').text(txt);
};

var test3 = function () {
    gGameEngine.onUpdate = updateDebugWindow;
    readyToRoll();
};


//jtd: taken from grits
var run = function () {
    gRenderEngine.context.clearRect(0, 0, gRenderEngine.canvas.width, gRenderEngine.canvas.height);
    gGameEngine.run();
    // if(mrdoob_stats)
    // mrdoob_stats.update();
    requestAnimFrame(run);
};


//jtd: i'm so original!!!
function readyToRoll() {
    //kick off our animation
    requestAnimFrame(run);
}
