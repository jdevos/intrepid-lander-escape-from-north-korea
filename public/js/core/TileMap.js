/*Copyright 2012 Google Inc. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 #limitations under the License.*/

var TileMapLoaderClass = Class.extend({
    assetManager: null,
    useOffscreenCanvas: true,
    currMapData: null,
    tileSets: new Array(),
    drawCollisionShapes: false,
    playerSpawnLocation: null,
    viewRect: {
        "x": 0,
        "y": 0,
        "w": 512,
        "h": 512,

        //to move the camera in the middle, calf offset of half w/h
        "cx": 0,
        "cy": 0
    },
    numXTiles: 100,
    numYTiles: 100,
    tileSize: {
        "x": 64,
        "y": 64
    },
    pixelSize: {
        "x": 64,
        "y": 64
    },
    preCacheCanvasArray: null,
    imgLoadCount: 0,
    init: function () {
        console.log('initialize tilemap');
    },

    setup: function () {
        var vw = this.viewRect;
        vw.cx = Math.floor(0 - (vw.w * 0.5));
        vw.cy = Math.floor(0 - (vw.h * 0.5));
    },

    load: function (map, imgPath) {
        //hack jtd: we should call setup when we know the final viewrect size
        this.setup();

        var _loading = $.Deferred();
        var _promise = _loading.promise();


        var _imgPath = imgPath ? imgPath : "img/";
        this.currMapData = map;
        this.numXTiles = map.width;
        this.numYTiles = map.height;
        this.tileSize.x = map.tilewidth;
        this.tileSize.y = map.tileheight;
        this.pixelSize.x = this.numXTiles * this.tileSize.x;
        this.pixelSize.y = this.numYTiles * this.tileSize.y;

        var mapInst = this;
        var updateLoadStats = function () {
            mapInst.imgLoadCount++;
            console.log("stats: loaded %s out of %s", mapInst.imgLoadCount, mapInst.tileSets.length)
            if (mapInst.imgLoadCount == map.tilesets.length) {
                console.log("all loaded");
                _loading.resolve();
            }
        };

        //load the tilesets
        for (var i = 0; i < map.tilesets.length; i++) {
            var img = new Image();
            img.onload = updateLoadStats;
            img.src = _imgPath + map.tilesets[i].image.replace(/^.*[\\\/]/, '');
            var ts = {
                "firstgid": map.tilesets[i].firstgid,
                "image": img,
                "imageheight": map.tilesets[i].imageheight,
                "imagewidth": map.tilesets[i].imagewidth,
                "name": map.tilesets[i].name,
                "numXTiles": Math.floor(map.tilesets[i].imagewidth / this.tileSize.x),
                "numYTiles": Math.floor(map.tilesets[i].imageheight / this.tileSize.y)
            };
            this.tileSets.push(ts);
        }

        //Once all the images are loaded, kick off the pre-caching system
        if (this.useOffscreenCanvas) {
            _promise.done(function () {
                mapInst.preDrawCache()
            });
        }
        return _promise;
    },

    getTilePacket: function (tileIndex) {
        if (tileIndex === 0) return null;
        var pkt = {
            "img": null,
            "px": 0,
            "py": 0
        };
        var i = 0;
        for (i = this.tileSets.length - 1; i >= 0; i--) {
            if (this.tileSets[i].firstgid <= tileIndex) break;
        }

        pkt.img = this.tileSets[i].image;
        var localIdx = tileIndex - this.tileSets[i].firstgid;
        var lTileX = Math.floor(localIdx % this.tileSets[i].numXTiles);
        var lTileY = Math.floor(localIdx / this.tileSets[i].numXTiles);
        pkt.px = (lTileX * this.tileSize.x);
        pkt.py = (lTileY * this.tileSize.y);

        return pkt;
    },
    intersectRect: function (r1, r2) {
        return !(r2.left > r1.right ||
            r2.right < r1.left ||
            r2.top > r1.bottom ||
            r2.bottom < r1.top);
    },

    quickDraw: function () {
        if (this.preCacheCanvasArray != null) {
            var r2 = this.viewRect;
            //aabb test to see if our view-rect intersects with this canvas.
            for (var q = 0; q < this.preCacheCanvasArray.length; q++) {
                var r1 = this.preCacheCanvasArray[q];
                var visible = this.intersectRect({top: r1.y, left: r1.x, bottom: r1.y + r1.h, right: r2.x + r2.w},
                    {top: r2.y, left: r2.x, bottom: r2.y + r2.h, right: r2.x + r2.w});

                if (visible)
                    gRenderEngine.context.drawImage(r1.preCacheCanvas, r1.x - this.viewRect.x, r1.y - this.viewRect.y);
            }
        }
    },

    draw: function () { //
        if (this.useOffscreenCanvas) {
            this.quickDraw();
            return;
        }
        //this._drawViewRect();


        for (var layerIdx = 0; layerIdx < this.currMapData.layers.length; layerIdx++) {
            var layer = this.currMapData.layers[layerIdx];

            if (layer.type != "tilelayer") continue;
            if (!layer.visible) continue;

            var dat = this.currMapData.layers[layerIdx].data;
            //find what the tileIndexOffset is for this layer
            for (var tileIDX = 0; tileIDX < dat.length; tileIDX++) {
                var tID = dat[tileIDX];
                if (tID == 0) continue;

                var tPKT = this.getTilePacket(tID);

                //test if this tile is within our world bounds
                var worldX = Math.floor(tileIDX % this.numXTiles) * this.tileSize.x;
                var worldY = Math.floor(tileIDX / this.numXTiles) * this.tileSize.y;
                if ((worldX + this.tileSize.x) < this.viewRect.x)continue;
                if ((worldY + this.tileSize.y) < this.viewRect.y)continue;
                if (worldX > this.viewRect.x + this.viewRect.w) continue;
                if (worldY > this.viewRect.y + this.viewRect.h) continue;

                //adjust all the visible tiles to draw at canvas origin.
                worldX -= this.viewRect.x;
                worldY -= this.viewRect.y;

                // Nine arguments: the element, source (x,y) coordinates, source width and
                // height (for cropping), destination (x,y) coordinates, and destination width
                // and height (resize).
                gRenderEngine.context.drawImage(tPKT.img, tPKT.px, tPKT.py, this.tileSize.x, this.tileSize.y, worldX, worldY,
                    this.tileSize.x, this.tileSize.y);
            }
        }

        //CLM used to help debugging
        if (this.drawCollisionShapes) {
            this.drawCollisionShapes();
        }

    },

    //debug: draw a border indicating what we think is the view-rect bounds
    //FIXME:  remove
    _drawViewRect: function () {
        var ctx = gRenderEngine.context;
        ctx.save();

        //determine current bounds of viewport
        var left = this.viewRect.x;
        var right = left + this.viewRect.w;
        var top = this.viewRect.x;
        var bottom = top + this.viewRect.h;
        ctx.strokeStyle = 'rgba(255,0,0,0.5)';
        ctx.strokeRect(0, 0, this.viewRect.w, this.viewRect.h);
        ctx.restore();

    },

    drawCollisionShapes: function () {
        //load our object and collision layers
        for (var layerIdx = 0; layerIdx < this.currMapData.layers.length; layerIdx++) {
            if (this.currMapData.layers[layerIdx].type != "objectgroup") continue;
            var lyr = this.currMapData.layers[layerIdx];
            var name = lyr.name;
            if (name == "collision") {
                //for each object, make a collision object
                for (var objIdx = 0; objIdx < lyr.objects.length; objIdx++) {
                    var lobj = lyr.objects[objIdx];
                    if (lobj.type != "") continue; //don't handle poly-lines just yet...
                    //test if this tile is within our world bounds
                    var worldX = lobj.x;
                    var worldY = lobj.y;

                    //adjust all the visible tiles to draw at canvas origin.
                    worldX -= this.viewRect.x;
                    worldY -= this.viewRect.y;

                    gRenderEngine.context.fillStyle = "white";
                    gRenderEngine.context.fillRect(worldX, worldY, lobj.width, lobj.height);

                }
            }
            //else WTF else are we doing right now?
        }
    },


    //jtd: pulled this from grits.   this algorithm is faster and uses offscreen canvases
    //      but that's about all i know... need to study up on it
    preDrawCache: function () {
        var divSize = 1024;
        this.preCacheCanvasArray = new Array();
        var xCanvasCount = 1 + Math.floor(this.pixelSize.x / divSize);
        var yCanvasCount = 1 + Math.floor(this.pixelSize.y / divSize);
        var numSubCanv = xCanvasCount * yCanvasCount;

        for (var yC = 0; yC < yCanvasCount; yC++) {
            for (var xC = 0; xC < xCanvasCount; xC++) {
                var k = {
                    x: xC * divSize,
                    y: yC * divSize,
                    w: Math.min(divSize, this.pixelSize.x),
                    h: Math.min(divSize, this.pixelSize.y),
                    preCacheCanvas: null};

                var can2 = document.createElement('canvas');
                can2.width = k.w;
                can2.height = k.h;

                k.preCacheCanvas = can2;
                this.preCacheCanvasArray.push(k);
            }
        }

        for (var cc = 0; cc < this.preCacheCanvasArray.length; cc++) {
            var can2 = this.preCacheCanvasArray[cc].preCacheCanvas;

            var ctx = can2.getContext('2d');

            ctx.fillRect(0, 0, this.preCacheCanvasArray[cc].w, this.preCacheCanvasArray[cc].h);
            var vRect = {    top: this.preCacheCanvasArray[cc].y,
                left: this.preCacheCanvasArray[cc].x,
                bottom: this.preCacheCanvasArray[cc].y + this.preCacheCanvasArray[cc].h,
                right: this.preCacheCanvasArray[cc].x + this.preCacheCanvasArray[cc].w};

            for (var layerIdx = 0; layerIdx < this.currMapData.layers.length; layerIdx++) {
                var layer = this.currMapData.layers[layerIdx];

                if (layer.type != "tilelayer") continue;
                if (!layer.visible) continue;

                var dat = this.currMapData.layers[layerIdx].data;
                //find what the tileIndexOffset is for this layer
                for (var tileIDX = 0; tileIDX < dat.length; tileIDX++) {
                    var tID = dat[tileIDX];
                    if (tID == 0) continue;

                    var tPKT = this.getTilePacket(tID);

                    //test if this tile is within our world bounds
                    var worldX = Math.floor(tileIDX % this.numXTiles) * this.tileSize.x;
                    var worldY = Math.floor(tileIDX / this.numXTiles) * this.tileSize.y;

                    var visible = this.intersectRect(vRect,
                        {top: worldY, left: worldX, bottom: worldY + this.tileSize.y, right: worldX + this.tileSize.x});
                    if (!visible)
                        continue;

                    // Nine arguments: the element, source (x,y) coordinates, source width and
                    // height (for cropping), destination (x,y) coordinates, and destination width
                    // and height (resize).
                    //		ctx.fillRect(worldX,worldY,this.tileSize.x, this.tileSize.y);

                    ctx.drawImage(tPKT.img,
                        tPKT.px, tPKT.py,
                        this.tileSize.x, this.tileSize.y,
                        worldX - vRect.left,
                        worldY - vRect.top,
                        this.tileSize.x, this.tileSize.y);


                }
            }
            this.preCacheCanvas = can2;
        }

    },

    //TODO: use an object layer in tiled to specify this
    getPlayerSpawn: function () {
        //find worldpos of middle tile, near the top
        var col = this.numXTiles / 2;
        var row = 2  //just guessing at best row for now

        var worldX = Math.floor(col * this.tileSize.x);
        var worldY = Math.floor(row * this.tileSize.y);

        //jtd: don't forget to scoot camera half-lenth to the left
        var wp = {x: worldX, y: worldY};
        console.log("getPlayerSpawn():: x:%s  y:%s", wp.x, wp.y);
        return wp;
    },

    //create static physics bodies from any layer named "collision" from the current mapdata
    setupCollisionLayers: function () {
        var collisionLayers = this.currMapData.layers.filter(function (layer) {
            var props = layer.properties;
            if (props) {
                if (props.objectType && props.objectType === "collision") {
                    return true;
                }
            }
            return layer.name === "collision"
        });
        for (var i = 0; i < collisionLayers.length; i++) {
            var layer = collisionLayers[i];
            for (var j = 0; j < layer.objects.length; j++) {
                var bodyInfo = layer.objects[j];
                //fixme: we can decouple this by  returning a list of the collision objects instead,  then have gameEngine dish it off to physics
                gPhysicsEngine.addStaticBody(bodyInfo);
            }

        }
    }


});
gTileMap = new TileMapLoaderClass();
