//FIXME: needs apache/google attribution

GameEngineClass = Class.extend({
    gMap: null,
    renderEngine: null,
    player: null,
    timeSinceGameUpdate: 0,
    timeSincePhysicsUpdate: 0,
    clock: null,
    onUpdate: null,
    debugPhysics: false,

    //todo not sure where this figures in yet
    fps: 0,
    currentTick: 0,
    lastFpsSec: 0,

    init: function (gMap, renderEngine) {
        console.log("init gameengine");
        this.gMap = gMap;
        this.renderEngine = renderEngine;
        this._deferredKill = [];
        this.clock = new TimerClass();

    },

    factory: new FactoryClass(),

    entities: [],

    setup: function () {
        console.log("GameEngine.setup()");

        if (this.debugPhysics) {
            gPhysicsEngine.setupDebug(this.renderEngine.context);
        }


        //FIXME: not initializing to correct size
        //FIXME: use constants should be same as canvas dimensions
        gTileMap.useOffscreenCanvas = true;
        gTileMap.viewRect.w = Constants.CANVAS_WIDTH;
        gTileMap.viewRect.h = Constants.CANVAS_HEIGHT;


        //add the collision objects (assuming only rectangles for now) to the box2d "world"
        //FIXME: add the ability to do polylines
        gTileMap.setupCollisionLayers()


        this._setupCamera();

        //initialize spritesqwer
        var spriteSheet = new SpriteSheetClass();
        spriteSheet.loadTpsData("/assets/ship-assets.png",
            gAssetLoader.cache["/assets/ship-assets.js"]);

        //init the ship
        this._setupPlayer();

        //now the keybindings
        this._setupKeybindings();

    },

    //don't let camera pan to area that extends beyond world
    _clamp: function (pos) {
        var pxsz = this.gMap.pixelSize;
        var vw = this.gMap.viewRect;
        vw.x = this.player.pos.x - (vw.w * .5) - this.player.hsize.w;
        vw.y = this.player.pos.y - (vw.h * .5) - this.player.hsize.h;

        var xmax = pxsz.x - vw.w;
        var ymax = pxsz.y - vw.h;

        if (vw.x < 0) vw.x = 0;
        if (vw.y < 0) vw.y = 0;

        if (vw.x > xmax) vw.x = xmax;
        if (vw.y > ymax) vw.y = ymax;
    },

    _setupKeybindings: function () {
        //bind ship controls
        var binds = {};
        binds[KEY.UP_ARROW] = 'camera-up',
            binds[KEY.RIGHT_ARROW] = 'camera-right';
        binds[KEY.DOWN_ARROW] = 'camera-down';
        binds[KEY.LEFT_ARROW] = 'camera-left';
        binds[KEY['E']] = 'ship-up';
        binds[KEY.F] = 'ship-right';
        binds[KEY.D] = 'ship-down';
        binds[KEY.S] = 'ship-left';
        binds[KEY.R] = 'ship-rot-right';
        binds[KEY.W] = 'ship-rot-left';
        gInputEngine.bindAll(binds);
    },

    //move camera to spawn location
    //FIXME: break this out into a _clampCamera method
    _setupCamera: function () {
        var spawnloc = gTileMap.getPlayerSpawn();
        var vw = this.gMap.viewRect;
        var pxsz = this.gMap.pixelSize;

        vw.x = spawnloc.x + vw.cx;
        vw.y = spawnloc.y + vw.cy;
    },

    _resetPlayerLocation: function () {
        this.player.loc = gTileMap.getPlayerSpawn();
    },


    //init player and then move player to spawn location
    _setupPlayer: function () {
        //hack jtd: magic words
        //also,  we should get ths stats and use that to drive the initialization
        //we are essentially initializing this thing twice...

        var spriteName = "shipa-large.png";
        var loc = gTileMap.getPlayerSpawn();
        var stats = gSpriteSheets["/assets/ship-assets.png"].getStats(spriteName);
        var settings = {
            size: {w: stats.w, h: stats.h},
            currSpriteName: spriteName
        };

        this.player = new LanderEntityClass(loc.x, loc.y, settings);
        this.entities.push(this.player);
    },

    //jtd this is ripped from GRITS.  I hope it works :-|
    run_old: function () {

        //jtd: Originally  GameEngine 'base' class
        this.fps++;
        GlobalTimer.step();

        var timeElapsed = this.clock.tick();
        this.timeSinceGameUpdate += timeElapsed;
        this.timeSincePhysicsUpdate += timeElapsed;

        while (this.timeSinceGameUpdate >= Constants.GAME_LOOP_HZ &&
            this.timeSincePhysicsUpdate >= Constants.PHYSICS_LOOP_HZ) {
            // JJG: We should to do a physics update immediately after a game update to avoid
            //      the case where we draw after a game update has run but before a physics update
            this.update();
            this.updatePhysics();
            this.timeSinceGameUpdate -= Constants.GAME_LOOP_HZ;
            this.timeSincePhysicsUpdate -= Constants.PHYSICS_LOOP_HZ;
        }

        while (this.timeSincePhysicsUpdate >= Constants.PHYSICS_LOOP_HZ) {
            // JJG: Do extra physics updates
            this.updatePhysics();
            this.timeSincePhysicsUpdate -= Constants.PHYSICS_LOOP_HZ;
        }

        if (this.lastFpsSec < this.currentTick / Constants.GAME_UPDATES_PER_SEC && this.currentTick % Constants.GAME_UPDATES_PER_SEC == 0) {
            this.lastFpsSec = this.currentTick / Constants.GAME_UPDATES_PER_SEC;
            this.fps = 0;
        }

        //jtd: Originally from run() override in ClientGameEngine.  we need to do both
        //		since we just have one GameEngine class
        var fractionOfNextPhysicsUpdate = this.timeSincePhysicsUpdate / Constants.PHYSICS_LOOP_HZ;

        this.update(); //CLM why was this removed?

        //gGuiEngine.draw();

        this.draw(fractionOfNextPhysicsUpdate);
        gInputEngine.clearPressed();
    },

    run: function () {
        this.update();
        this.draw();
        gInputEngine.clearPressed();
    },

    updateInput: function () {
        var body = this.player.physBody,
            ship = this.player,
            shipspeed = 2,
            rotspeed = 60,
            impRads = Math.PI / 2,
            fuelCostMain = 10,
            fuelCostStab = 1;

        //unit vector in direction of rotation

        var rot = ship.rot - (Math.PI / 2);

        var imp = new Vec2(Math.cos(rot), Math.sin(rot));
        imp.Multiply(shipspeed);
        var pos = body.GetPosition();
        pos.Multiply(gPhysicsEngine.scale);

        //run through bindings....
        function stat(action) {
            return gInputEngine.state(action);
        }

        if (stat('ship-up') && ship.fuel > 0) {
            body.ApplyImpulse(imp, pos);
            ship.fuel -= fuelCostMain;

        } else {
            body.ApplyImpulse({x: 0, y: 0}, pos);
        }

        if (stat('ship-right') && ship.fuel > 0) {
            body.SetAngularVelocity(impRads);
            ship.fuel -= fuelCostStab;
        } else if (stat('ship-left') && ship.fuel > 0) {
            //console.log("torque left")
            //body.ApplyTorque(-rotspeed);
            body.SetAngularVelocity(-impRads);
            ship.fuel -= fuelCostStab;
        } else if (stat('ship-down')) body.SetAngularVelocity(0)
    },

    update: function () {
        this.updateInput();
        gPhysicsEngine.update();

        this.entities.forEach(function (ent) {
            ent.update();  //noop if entity is dead
        });

        //todo: other stuff goes here?
        this.removeDeadEntities();

        this._clamp();


        //hook so I can update debug instruments and whatnot
        //todo: use jquery custom events so error in listener doesn't crash us
        if (this.onUpdate) {
            this.onUpdate();
        }
    },

    removeDeadEntities: function () {
        var ent;
        while (ent = this._deferredKill.shift()) {
            //do some cleanup on dead entity (remove from box2d maybe?)
        }

    },

    draw: function (df) {
        var gMap = this.gMap;
        // Draw map. Note that we're passing a canvas context
        // of 'null' in. This would normally be our game context,
        // but we don't need to grade this here.
        this.gMap.draw();

        // Bucket entities by zIndex
        var fudgeVariance = 128;
        var zIndex_array = [];
        var entities_bucketed_by_zIndex = {};
        var bents = entities_bucketed_by_zIndex;
        gGameEngine.entities.forEach(function (entity) {
            //don't draw entities that are off screen
            if (entity.pos.x >= gMap.viewRect.x - fudgeVariance &&
                entity.pos.x < gMap.viewRect.x + gMap.viewRect.w + fudgeVariance &&
                entity.pos.y >= gMap.viewRect.y - fudgeVariance &&
                entity.pos.y < gMap.viewRect.y + gMap.viewRect.h + fudgeVariance) {
                // Bucket the entities in the entities list by their zindex
                // property.
                if (!bents[entity.zindex]) {
                    zIndex_array.push(entity.zindex);
                    bents[entity.zindex] = [];
                }
                bents[entity.zindex].push(entity);
            }
        });
        zIndex_array.sort();


        // Draw entities sorted by zIndex
        zIndex_array.forEach(function (i) {
            var ents = bents[i];
            ents.forEach(function (ent) {
                ent.draw();
                //console.log("drawing %s at zindex %s", ent, i);
            });

        });

        if (this.debugPhysics) {
            gPhysicsEngine.world.DrawDebugData();
        }
    },

    //fixme:
    drawEntity: function (ent) {
        var settings = null;
        if (ent.rot !== 0) {
            settings = {rotRadians: ent.rot}
        }
        drawSprite(ent.currSpriteName, ent.pos.x, ent.pos.y, settings);
    },


    _onScreen: function (entity) {

    },


    spawnEntity: function (typename) {
        var instance = this.factory.createInstance(typename);
        if (instance === null) {
            console.log("Can't spawn %s: class not registered w/ factory", typename);
        } else {
            this.entities.push(instance);
        }
        return instance;
    },


    setupGameBorders: function () {
        var ps = this.gMap.pixelSize;
        gPhysicsEngine.addBorders(0, 0, ps.x, ps.y);
    }
});

gGameEngine = new GameEngineClass(gTileMap, gRenderEngine);