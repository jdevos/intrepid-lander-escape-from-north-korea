(function(window, EntityClass, gPhysicsEngine, gAssetLoader) {

    window.LanderEntityClass = EntityClass.extend({
        vel: new Vec2(),
        rvel: 0.0,
        physBody: null,
        zindex: 10,

        //fuel used to power thruster.  Amount used based upon force of thrust times the number of ticks force is applied
        fuel: 1000.0,

        init: function (x, y, settings) {
            this.parent(x, y, settings);

            var entityData = gAssetLoader.cache["/assets/shipa-large.js"];
            console.dir(entityData);

            var guid = newGuid_short();
            var entityDef = {
                id: "lander" + guid,
                x: this.pos.x,
                y: this.pos.y,
                polyPoints: entityData.points,
                damping: 0,
                angularDamping: 1,
                angle: this.rot,
                useBouncyFixture: false,
                // categories: ['projectile', owningPlayer.team == 0 ? 'team0' : 'team1'],
                // collidesWith: ['mapobject', owningPlayer.team == 0 ? 'team1' : 'team0'],
                userData: {
                    "id": "lander" + guid,
                    "ent": this
                },

                //stuff applied to fixture;
                density: 0.01,
                friction: 0.5,
                restitution: 0.1
            };

            this._entityDef = entityDef;

            this.physBody = gPhysicsEngine.addBody(entityDef);
            //this.physBody = gPhysicsEngine.addSimpleBody(entityDef);
        },

        update: function () {
            if (this._killed) return false;
            if (this.physBody) {
                this.pos = this.physBody.GetPosition();
                this.pos.x /= gPhysicsEngine.scale;
                this.pos.y /= gPhysicsEngine.scale;
                this.rot = this.physBody.GetAngle();
            }
        }
    });
})(window, EntityClass, gPhysicsEngine, gAssetLoader);
