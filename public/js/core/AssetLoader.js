AssetLoaderClass = Class.extend({
    cache: {},
    init: function () {
    },
    getFileType: function (filename) {
        function t(re) {
            return re.test(filename);
        }

        if (t(/png$/i) || t(/jpg$/i) || t(/gif$/i)) return "img";
        if (t(/json$/i)) return "json";
        //FIXME: treat js and json the same for now - we don't plan on dynamically loading js files.
        if (t(/js$/i)) return "json";

        if (t(/ogg$/i) || t(/aac$/i)) return "audio";
        return "unknown"
    },

    loadAssets: function (urls) {
        var self = this;
        var deferred = $.Deferred();
        var totalAssets = urls.length;
        var assetCount = 0;
        var load = {
            "audio": function (url, cb) {
                xhrGet(url, {
                    responseType: "arraybuffer"
                }, function (xhr) {
                    var buffer = xhr.response;
                    self.cache[url] = buffer;
                    cb(url);
                });
            },
            "img": function (url, cb) {
                var img = new Image();
                img.onload = function () {
                    cb(url);
                };
                self.cache[url] = img;
                img.src = url;
            },
            "js": function (url, cb) {
                //TODO: just use script tags for now
            },
            "json": function (url, cb) {
                xhrGet(url, {}, function (xhr) {
                    var json = JSON.parse(xhr.response);
                    self.cache[url] = json;
                    cb(url);
                });
            }
        };

        var assetLoaded = function (url) {
            deferred.notify("loaded file:" + url);
            assetCount++;
            if (assetCount === totalAssets) {
                //give the assets to any promise listeners
                deferred.resolve(self.cache);
            }
        };

        for (var i = 0; i < urls.length; i++) {
            var url = urls[i];
            if (self.cache[url]) {
                //if it's in the cache, call the callback immediately
                assetLoaded(url);
            } else {
                var type = this.getFileType(url);
                load[type](url, assetLoaded);
            }
        }
        return deferred.promise();
    },

});

gAssetLoader = new AssetLoaderClass();
