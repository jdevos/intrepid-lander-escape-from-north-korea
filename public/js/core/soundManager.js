//requires: jquery, assetmanager
//http://www.html5rocks.com/en/tutorials/webaudio/intro/
var AudioContext = window.AudioContext || window.webkitAudioContext;

//retuires: jQuery,  gAssetManager
var SoundManager = Class.extend({
    _context: null,
    _mainNode: null,
    init: function () {
        console.log("new soundmanager");
    },
    clips: {},
    enabled: true,
    create: function () {
        try {
            this._context = new AudioContext();
            console.log("context created");
        } catch (e) {
            window.alert("could not init context:" + e);
        }
        if (this._context === null) return;

        this._mainNode = this._context.createGainNode(0);
        this._mainNode.connect(this._context.destination);
    },

    addClip: function (name, url) {
        var self = this;
        console.log("addding clip name:%s   url:%s", name, url);
        var clip = {sound: null, buffer: null, loaded: false};
        this.clips[name] = clip;

        var promise = gAssetLoader.loadAssets([url]);

        promise.done(function () {
            console.log("loaded sound file: %s", url);
            self._context.decodeAudioData(
                gAssetLoader.cache[url],
                function _success(audiobuffer) {
                    clip.buffer = audiobuffer;
                    clip.loaded = true;
                },
                function _failed(err) {
                    console.error("failed to decode clip: %s", err);
                }
            );
        });

    },

    playClip: function (name) {
        var clip = this.clips[name];
        if (!clip.loaded) {
            console.log("cannot play %s, not loaded yet", name);
        } else {
            var source = this._context.createBufferSource();
            source.buffer = clip.buffer;
            source.connect(this._context.destination);
            source.start(0);

        }
    }

});


