/*Copyright 2011 Google Inc. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 #limitations under the License.*/

SpriteSheetClass = Class.extend({
    img: null,
    url: "",
    sprites: new Array(),
    renderEngine: null,
    imageCache: null,

    //-----------------------------------------
    init: function () {
        this.renderEngine = gRenderEngine;
        this.imageCache = gAssetLoader.cache;
    },


    //load the atlas image
    //TODO: use asset manager instead
    load: function (imageUrl) {
        this.img = this.imageCache[imageUrl];
        this.url = imageUrl;
    },

    //-----------------------------------------
    defSprite: function (name, x, y, w, h, cx, cy) {
        var spt = {
            "id": name,
            "x": x,
            "y": y,
            "w": w,
            "h": h,
            "cx": cx == null ? 0 : cx,
            "cy": cy == null ? 0 : cy
        };
        this.sprites.push(spt);
    },

    //-----------------------------------------
    getStats: function (name) {
        for (var i = 0; i < this.sprites.length; i++) {
            if (this.sprites[i].id == name) return this.sprites[i];
        }
        return null;
    },

    //load an atlas image and define the sprites defined in the texturepacker data
    loadTpsData: function (imgName, atlasData) {
        this.load(imgName);
        var frames = atlasData.frames;
        for (var spriteName in frames) {
            var sprite = frames[spriteName];

            //define center of sprite as offset
            var cx = -sprite.frame.w * 0.5;
            var cy = -sprite.frame.h * 0.5;

            if (sprite.trimmed) {
                cx = (-sprite.sourceSize.w * 0.5) + sprite.spriteSourceSize.x;
                cy = (-sprite.sourceSize.h * 0.5) + sprite.spriteSourceSize.y;
            }


            this.defSprite(spriteName, sprite.frame.x, sprite.frame.y, sprite.frame.w,
                sprite.frame.h, cx, cy);
        }
        gSpriteSheets[imgName] = this;
    },

    drawSprite: function (spriteName, worldX, worldY) {
        // Use the getStats method of the spritesheet
        // to find if a sprite with name 'spritename'
        // exists in that sheet...
        var sprite = this.getStats(spriteName);

        __drawSpriteInternal(sprite, this, worldX, worldY);
    }


});




