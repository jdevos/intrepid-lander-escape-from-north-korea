/*Copyright 2012 Google Inc. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 #limitations under the License.*/

FactoryClass = Class.extend({
    nameClassMap: {},

    init: function (map) {
        if (typeof map === "object") {
            for (var name in map) {
                this.register(name, map[name]);
            }
        }
    },

    getClass: function (name) {
        return this.nameClassMap[name];
    },

    register: function (name, Clazz) {
        this.nameClassMap[name] = Clazz;
    },

    //optional constructor args not supported, but will pass to instance.setup() instead if it exists
    createInstance: function () {
        var name = arguments[0];
        var Clazz = this.nameClassMap[name];
        if (!Clazz) return null;
        var instance = new Clazz();
        if (arguments.length > 1 && Clazz.prototype.setup) {
            var setupargs = Array.prototype.splice.call(arguments, 1);
            Clazz.prototype.setup.apply(instance, setupargs)
        }
        return instance;
    }
});
