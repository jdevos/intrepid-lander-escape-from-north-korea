EntityClass = Class.extend({
    _con: {log: function () {
        console.log.apply(console, arguments);
    }},
    _nullcon: {log: function(){}},
    loggingEnabled: true,
    id: 0,
    name: "an entity",
    pos: new Vec2(),
    size: {w: 0, h: 0},
    hsize: {w: 0, h: 0},
    last: new Vec2(),
    rot: 0.0,
    currSpriteName: null,
    zindex: 0,

    init: function (x, y, _settings) {
        if (!this.loggingEnabled) this._con = _nullcon;
        console.log("entity init:: x:%s  y:%s", x, y);
        var settings = $.extend({}, _settings);
        this.pos.x = x;
        this.pos.y = y;
        if (settings.size) {
            this.size.w = settings.size.w;
            this.size.h = settings.size.h;
            this.hsize.w = this.size.w * 0.5;
            this.hsize.h = this.size.h * 0.5;
        }
        if (settings.currSpriteName) {
            this.currSpriteName = settings.currSpriteName;
        }
        this.id = EntityClass._nextId++;
        this._killed = false;
    },

    setup: function (name) {
    },

    draw: function () {
        if (this.currSpriteName) {
            drawSprite(this.currSpriteName,
                //FIXME: sometimes we round, sometimes we dont... figure out which is right
                Math.round(this.pos.x),
                Math.round(this.pos.y),
                {rotRadians: this.rot});
        }
    },

    update: function () {
        if (this._killed) return false;
        return true;
    },
});
EntityClass._nextId = 0;
