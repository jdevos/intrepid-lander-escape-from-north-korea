/*Copyright 2012 Google Inc. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 #limitations under the License.*/

//Box2D = box2D.Box2D;

Vec2 = Box2D.Common.Math.b2Vec2;
BodyDef = Box2D.Dynamics.b2BodyDef;
Body = Box2D.Dynamics.b2Body;
FixtureDef = Box2D.Dynamics.b2FixtureDef;
Fixture = Box2D.Dynamics.b2Fixture;
World = Box2D.Dynamics.b2World;
MassData = Box2D.Collision.Shapes.b2MassData;
PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
CircleShape = Box2D.Collision.Shapes.b2CircleShape;
EdgeShape = Box2D.Collision.Shapes.b2EdgeShape;

DebugDraw = Box2D.Dynamics.b2DebugDraw;
RevoluteJointDef = Box2D.Dynamics.Joints.b2RevoluteJointDef;

//FIXME: hide this function in SIAF
function _fixtureDef(shape) {
    var def = new FixtureDef();
    // def.friction = 0;
    // def.density = 0.001;
    // def.restitution = 0.3;

    def.shape = shape;
    return def;
};


PhysicsEngineClass = Class.extend({
    world: null,
    debugDraw: null,

    //box2d docs advise that moving objects be between 0-10 meters (50 meters for static objects)
    scale: 1.0,

    //-----------------------------------------
    init: function (settings) {
        var defaults = {
            gravity: new Vec2(0, Constants.PHYSICS_DEFAULT_GRAVITY),
            allowSleep: Constants.PHYSICS_ALLOW_IDLE_SLEEP,
            initialScale: 1.0,
            b2_maxTranslation: 99999,

            //adjust scale based on the largest incoming object as it is introduced to the world
            autoAdjustScale: false,

        };
        var opts = core_extend({}, defaults);
        if (settings) {
            core_extend(opts, settings);
        }

        if (!World) {
            console.error("Woeld no go");
        }
        this.world = new World(opts.gravity, opts.allowSleep);
        this.scale = opts.initialScale;
        Box2D.Common.b2Settings.b2_maxTranslation = opts.b2_maxTranslation;
        Box2D.Common.b2Settings.b2_maxTranslationSquared = Box2D.Common.b2Settings.b2_maxTranslation * Box2D.Common.b2Settings.b2_maxTranslation;

    },


    setupDebug: function (context2d) {
        //setup debug draw
        var debugDraw = new DebugDraw();
        debugDraw.SetSprite(context2d);
        debugDraw.SetDrawScale(0.2);
        debugDraw.SetFillAlpha(0.3);
        debugDraw.SetLineThickness(1.0);
        debugDraw.SetFlags(DebugDraw.e_shapeBit | DebugDraw.e_jointBit);
        this.world.SetDebugDraw(debugDraw);
        this.debugDraw = debugDraw;
    },

    //-----------------------------------------
    addContactListener: function (callbacks) {
        var listener = new Box2D.Dynamics.b2ContactListener;
        if (callbacks.BeginContact) listener.BeginContact = function (contact) {
            callbacks.BeginContact(contact.GetFixtureA().GetBody(), contact.GetFixtureB().GetBody());
        }
        if (callbacks.EndContact) listener.EndContact = function (contact) {
            callbacks.EndContact(contact.GetFixtureA().GetBody(), contact.GetFixtureB().GetBody());
        }
        if (callbacks.PostSolve) listener.PostSolve = function (contact, impulse) {
            callbacks.PostSolve(contact.GetFixtureA().GetBody(), contact.GetFixtureB().GetBody(), impulse.normalImpulses[0]);
        }
        this.world.SetContactListener(listener);
    },
    //-----------------------------------------
    update: function () {
        var start = Date.now();
        //frame-rate
        //velocity iterations
        //position iterations
        this.world.Step(Constants.PHYSICS_LOOP_HZ, 10, 10);
        return (Date.now() - start);
    },

    //clear any forces on the dynamic entities.
    clearForces: function () {
        this.world.ClearForces();
    },
    //-----------------------------------------
    registerBody: function (bodyDef) {
        console.log('PhysicsEngine:: registering body: %s', bodyDef);
        var body = this.world.CreateBody(bodyDef);
        return body;
    },

    //  //-----------------------------------------
    addBody: function (entityDef) {
        var bodyDef = new BodyDef;

        var id = entityDef.id;

        if (entityDef.type == 'static') {
            bodyDef.type = Body.b2_staticBody;
        } else {
            bodyDef.type = Body.b2_dynamicBody;
        }

        //FIXME: these IF's are broken if the values are falsy
        bodyDef.position.x = entityDef.x * this.scale;
        bodyDef.position.y = entityDef.y * this.scale;
        if (entityDef.userData !== undefined) bodyDef.userData = entityDef.userData;
        if (entityDef.angle !== undefined) bodyDef.angle = entityDef.angle;
        if (entityDef.damping !== undefined) bodyDef.linearDamping = entityDef.damping;
        if (entityDef.angularDamping !== undefined) bodyDef.angularDamping = entityDef.angularDamping;

        //this should clean up a lot of these if statements!
        //$.extend(bodyDef, entityDef);

        var body = this.registerBody(bodyDef);

        var fixtureDefinition = new FixtureDef;
        if (entityDef.useBouncyFixture) {
            fixtureDefinition.density = 1.0;
            fixtureDefinition.friction = 0;
            fixtureDefinition.restitution = 1.0;
        }
        else {
            fixtureDefinition.density = 1.0;
            fixtureDefinition.friction = 0; //0.5;//0.0;
            fixtureDefinition.restitution = 0; //0.2;
        }

        if (entityDef.density !== undefined) fixtureDefinition.density = entityDef.density;
        if (entityDef.friction !== undefined) fixtureDefinition.friction = entityDef.friction;
        if (entityDef.restitution !== undefined) fixtureDefinition.restitution = entityDef.restitution;

        if (entityDef.radius) {
            fixtureDefinition.shape = new CircleShape(entityDef.radius);
            body.CreateFixture(fixtureDefinition);
        } else if (entityDef.polyPoints) {
            var points = entityDef.polyPoints;
            var vecs = [];
            for (var i = 0; i < points.length; i++) {
                var vec = new Vec2();
                vec.Set(points[i].x * this.scale, points[i].y * this.scale);
                vecs[i] = vec;
            }
            fixtureDefinition.shape = new PolygonShape;
            fixtureDefinition.shape.SetAsArray(vecs, vecs.length);
            body.CreateFixture(fixtureDefinition);

        }
        else { //we're a box!
            fixtureDefinition.shape = new PolygonShape;
            fixtureDefinition.shape.SetAsBox(entityDef.halfWidth, entityDef.halfHeight);
            body.CreateFixture(fixtureDefinition);
        }

        return body;

    },

    //to hades with all your fancy-schmancy collision-group poleygon body fibildy-foo!
    //this is how we defined it in the class, and I'm going with that!
    //FIXME: this doesn't properly copy all the right fields from entityDef to bodyDef
    addSimpleBody: function (entityDef) {
        // YOUR CODE HERE
        // Create a new BodyDef object
        var bodyDef = new BodyDef();

        // Specify the 'type' member of your
        // BodyDef object as Body.b2_staticBody
        // or Body.b2_dynamicBody based on the
        // 'type' member of the passed-in
        // entityDef object.

        if (entityDef.type == 'static') {
            bodyDef.type = Body.b2_staticBody;
        } else {
            bodyDef.type = Body.b2_dynamicBody;
        }

        //console.log("bodyDef****");
        //console.dir(bodyDef);


        // Set the position{x,y} member object
        // of your BodyDef object based on the
        // 'x' and 'y' members of the passed-in
        // entityDef object.
        bodyDef.position.x = entityDef.x;
        bodyDef.position.y = entityDef.y;

        bodyDef.userData = entityDef.userData;


        // call registerBody with your BodyDef
        // object to attach it to our world.
        var body = this.registerBody(bodyDef);

        // Create a new FixtureDef object
        var fixtureDef = new FixtureDef();

        if (entityDef.useBouncyFixture) {
            // Set your FixtureDef object's
            // 'density', 'friction', and
            // 'restitution' members to
            // 1.0, 0, and 1.0 respectively.
            // fixtureDef.density = 1.0;
            // fixtureDef.friction = 0;
            // fixtureDef.restitution = 1.0;
            fixtureDef.friction = 0;
        }
        //FIXME: actually figure this out.  I've been fiddling with this
        // as well as the impulse power
        //  so that i can get the stupid thing to turn *and* thrust forward
        fixtureDef.density = 0.001;
        fixtureDef.restitution = 0.3;

        // Create a new PolygonShape object, set your
        // FixtureDef object's 'shape' member to this
        // PolygonShape.
        var shape = new PolygonShape();
        fixtureDef.shape = shape;

        // Set the FixtureDef object's shape to a box using
        // the shape's SetAsBox method, which takes half the
        // width and half the height of the box as parameters.
        // You should get these from the passed-in entityDef,
        // which has 'halfWidth' and 'halfHeight' properties
        // that specify this.
        shape.SetAsBox(entityDef.halfWidth, entityDef.halfHeight);

        // Call the CreateFixture method of your BodyDef
        // object with your FixtureDef object as a parameter
        // to attach the fixture to your BodyDef object.

        // Return your BodyDef object.
        body.CreateFixture(fixtureDef);

        return body;
    },

    removeBody: function (objBody) {
        //console.log("PhysicsEngine:: destroying body id:%s", objBody.m_userData.id);
        this.world.DestroyBody(objBody)
    },

    //FIXME: remove
    //-----------------------------------------
    removeBodyAsObj: function (obj) {
        ///console.log("physicsEngine:: destroying:%s", obj);
        this.world.DestroyBody(obj);
    },

    //-----------------------------------------
    getBodySpec: function (b) {
        return {
            x: b.GetPosition().x,
            y: b.GetPosition().y,
            a: b.GetAngle(),
            c: {
                x: b.GetWorldCenter().x,
                y: b.GetWorldCenter().y
            }
        };
    },
    //-----------------------------------------
    applyImpulse: function (body, degrees, power) {
        body.ApplyImpulse(new Vec2(Math.cos(degrees * (Math.PI / 180)) * power, Math.sin(degrees * (Math.PI / 180)) * power), body.GetWorldCenter());
    },
    //-----------------------------------------
    clearImpulse: function (body) {
        body.m_linearVelocity.SetZero();
        body.m_angularVelocity = 0.0;
    },
    //-----------------------------------------
    setVelocity: function (bodyId, x, y) {
        var body = this.bodiesMap[bodyId];
        body.SetLinearVelocity(new Vec2(x, y));
    },
    //-----------------------------------------
    getVelocity: function (body) {
        return body.GetLinearVelocity();
    },
    //-----------------------------------------
    getPosition: function (body) {
        return body.GetPosition();
    },
    //-----------------------------------------
    setPosition: function (body, pos) {
        body.SetPosition(pos);
    },

    //TODO: add polyline support.  for now we just assume it's all rectangles.
    //TODO: handle the 'special' object types:  InstantDeath, LandingPad
    addStaticBody: function (tmxObj) {
        if (tmxObj.polygon) return false;

        //we're a rectangle
        var entityDef = {
            id: tmxObj.name,
            x: (tmxObj.x + (tmxObj.width * 0.5)) * this.scale,
            y: (tmxObj.y + (tmxObj.height * 0.5)) * this.scale,
            halfHeight: tmxObj.height * 0.5 * this.scale,
            halfWidth: tmxObj.width * 0.5 * this.scale,
            dampen: 0,
            angle: 0,
            type: 'static',
            // categories: collisionTypeArray,
            // collidesWith: collidesWithArray,
            userData: {
                "id": tmxObj.name
            },
        };
        this.addSimpleBody(entityDef);
    },

    getScaledPosition: function (pt) {
        return {
            x: pt.x * this.scale,
            y: pt.y * this.scale
        }
    },

    //add a static hollow box around play area
    addBorders: function (x1, y1, x2, y2) {
        x1 *= this.scale;
        y1 *= this.scale;
        x2 *= this.scale;
        y2 *= this.scale;

        var upperLeft = new Vec2(x1, y1);
        var upperRight = new Vec2(x2, y1);
        var lowerLeft = new Vec2(x1, y2);
        var lowerRight = new Vec2(x2, y2);

        var shapeLeft = new EdgeShape(upperLeft, lowerLeft);
        var shapeRight = new EdgeShape(upperRight, lowerRight);
        var shapeTop = new EdgeShape(upperLeft, upperRight);
        var shapeBottom = new EdgeShape(lowerLeft, lowerRight);

        var bodyDef = new BodyDef();
        bodyDef.position.x = 0;
        bodyDef.position.y = 0;
        bodyDef.userData = {id: "game.border"};
        var body = this.registerBody(bodyDef);

        body.CreateFixture(_fixtureDef(shapeLeft));
        body.CreateFixture(_fixtureDef(shapeRight));
        body.CreateFixture(_fixtureDef(shapeTop));
        body.CreateFixture(_fixtureDef(shapeBottom));
    }


});

var gPhysicsEngine = new PhysicsEngineClass({initialScale: 1.0 / 10.0});