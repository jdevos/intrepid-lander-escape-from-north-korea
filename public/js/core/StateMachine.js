//Game State Management
//FIXME: mostly pseudocode for now.

function StateMachine() {
	var states =  ["SPLASH", "INTRO", "PLAYING", "PAUSED", "GAMEOVER"];
	this.init();
}

StateMachine.prototype = {
	init: function() {},

	//tell state machine to transition to specified state
	changeState: function(newState) {

	},

	//register a listener to a particular state
	addListener: function(state, listener) {

	},

	//return the current state 
	currentState: function() {
	}
};