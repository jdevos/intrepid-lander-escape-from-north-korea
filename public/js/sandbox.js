	$(function() {
	    var sm = new SoundManager();
	    sm.create();
	    sm.addClip("boom", "/assets/audio/boom.ogg");

	    $('#btnPlay').click(function() {
	        console.log("click");
	        sm.playClip("boom");
	    });


	    //register the DeviceOrientation class=
	    var devo = new DeviceOrientationClass(window, true);

	    window.devo = devo;
	});
